function [X1,X2] = ForwardPropagate(W1,W2,f1,f2,trn_input)%% Mechanical Engineering Department% Copyright (c) 2001 The University of Texas at Austin%            �  2001 Benito Fernandez%%---------------------------------------------------------% Forward pass%---------------------------------------------------------X1 = feval(f1,W1*[trn_input; trn_one]);		% Output of layer 1 (hidden layer)X2 = feval(f2,W2*[X1; trn_one]) ;			% Output of layer 2 (output layer)%%	End of BackPropagate_Gradient%