This directory contains PDP++ Software files for downloading.

The current version of the software is 3.0, released April, 2003.

***Please read the INSTALL file for important info***

For more information, see our web page:
  WWW Page:   http://www.cnbc.cmu.edu/Resources/PDP++/PDP++.html
    -or-      http://psych.colorado.edu/~oreilly/PDP++/PDP++.html


============================
BINARIES (end user version):
============================

Most users will want to just grab the binary (pre-compiled) versions.
There are different forms of packaging depending on the platform.

-----
Unix:
-----

Files are distributed as .tar.gz files (compressed tar files, see
INSTALL for how to open).  However, an .rpm is also avial for Linux.

You need to grab 2 files: the _bin_ binaries and the _ext_ "extra"
stuff that is platform-independent:

pdp++_version_bin_CPU.tar.gz
pdp++_version_ext.tar.gz

where version is the version number of the software release, and
CPU is the cpu-type of the system you will be running on:

LINUX     An Intel 386-Pentium machine running a modern glibc version of
	  Linux (e.g., RedHat 6.x).
LINUXPPC  An Apple Mac running Linux PPC (modern glibc version, same as
	  LINUX).
CYGWIN    An Intel 386-Pentium machine running the Windows operating
	  system (using the Cygnus CygWin system as a compilation
	  environment).
DARWIN	  A Mac running OS-X (aka Darwin), which is based on FreeBSD.
	  Binaries are for the power PC (PPC) architecture.
SUN4      A SUN sparc-station system running a modern 5.x Solaris version
	  of the operating system.
SGI       A Silicon Graphics workstation running a recent Irix 6.x release.
HP800     A Hewlett Packard workstation running HP-UX version 10.x.
IBMaix    An IBM RS/6000 machine running AIX v4.1.4 (4.1.x should work)

There are also 'stat' versions for some platforms (e.g., LINUXstat),
which are statically linked executables.  These have all of the pdp++
specific libraries included in each executable, meaning that you don't
have to worry about the extra configuration steps for using shared
libraries.  However, these files are much larger and thus much less
efficient.  You decide.

----------
Linux RPM:
----------

For Linux, there is an .rpm distribution that makes for easier
installation.  It includes both the binaries and the "extra" stuff
(_bin_ and _ext_ tar files).  It is named:

pdp++-binext-version.i386.rpm

Just change to super-user and do:
rpm -Uvh pdp++-binext-version.i386.rpm 
to install.  It will not install as a regular user.

-----------
MS Windows:
-----------

A self-extracting executable is available for the Microsoft windows
version (9x and up, not 3.1).  This will run a standard setup/install
kind of program, and contains both the binaries and "extra" stuff.  It
is named:

pdp++_version_setup.exe

Just download and double-click on it. 

-----------
Mac OSX:
-----------

A self-installing package file is available for the Mac OSX.  It also
contains binaries and extra stuff.  It is named:

pdp++_version_osx.pkg.sig

Just click on it to install.  It installs into Applications/PDP++ and
/usr/local/pdp++, with links in /usr/bin.  You must have installed and
run an X11 server before the programs will run (they can be run just
by double-clicking).


==================================
SOURCE CODE (programmers version):
==================================

The source code is in the following file (includes "extras" too):

pdp++_version_src.tar.gz

Also, there are files for interviews (the graphical toolkit).  The
entire source for interviews is:

iv-15.tar.gz

(note that v.3.0 of pdp++ requires this new version of interviews).

There are also pre-compiled interviews libraries and the header files,
so you don't need to compile interviews yourself:

pdp++_version_ivlib_CPU_compiler.tar.gz (CPU = platform, compiler = CC or g++)
pdp++_version_ivinc.tar.gz (headers)

-----------
MS Windows:
-----------

The cygwin environment was used to compile pdp++ under ms windows.
See the cygwin directory for more info (cygwin/README).

==============
Documentation:
==============

See the docs directory for postscript and pdf versions of the manual.
